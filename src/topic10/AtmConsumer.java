package topic10;

import java.util.concurrent.atomic.AtomicBoolean;

public class AtmConsumer implements Runnable {
    private final Atm atmMoneyConsumer;
    private final AtomicBoolean running = new AtomicBoolean(false);

    public AtmConsumer(Atm atmConsumer) {
        this.atmMoneyConsumer = atmConsumer;
    }

    @Override
    public void run() {
        running.set(true);
        while (running.get()) {
            try {
                subtract(-50);
            } catch (CardException e) {
                Thread.currentThread().interrupt();
                running.set(false);
            }
        }
    }

    private synchronized void subtract(int money) throws CardException {
        atmMoneyConsumer.withdrawMoney(money);
        System.out.println("Consumer"
                + Thread.currentThread().getName()
                + " withdrew 50$. Card account balance : " + atmMoneyConsumer.getCard().getBalance());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public void close() {
        running.set(false);
    }
}

