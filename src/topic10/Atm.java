package topic10;

import java.util.concurrent.atomic.AtomicInteger;

public class Atm {
    private final Card card;

    public Atm(Card card) {
        this.card = card;
    }

    public synchronized AtomicInteger addMoney(int moneyForAddToCard) throws CardException {
        return card.addMoney(moneyForAddToCard);
    }

    public synchronized AtomicInteger withdrawMoney(int moneyForWithdraw) throws CardException {
        return card.withdrawMoney(moneyForWithdraw);
    }

    public synchronized Card getCard() {
        return card;
    }
}
