package topic10;

import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    public static void main(String[] args) {
        Card card = new Card(new AtomicInteger(500));

        Atm atmProducerOne = new Atm(card);
        Atm atmProducerTwo = new Atm(card);
        Atm atmProducerThree = new Atm(card);

        Atm atmConsumerOne = new Atm(card);
        Atm atmConsumerTwo = new Atm(card);
        Atm atmConsumerThree = new Atm(card);

        AtmProducer atmMoneyProducerOne = new AtmProducer(atmProducerOne);
        AtmProducer atmMoneyProducerTwo = new AtmProducer(atmProducerTwo);
        AtmProducer atmMoneyProducerThree = new AtmProducer(atmProducerThree);

        AtmConsumer atmMoneyConsumerOne = new AtmConsumer(atmConsumerOne);
        AtmConsumer atmMoneyConsumerTwo = new AtmConsumer(atmConsumerTwo);
        AtmConsumer atmMoneyConsumerThree = new AtmConsumer(atmConsumerThree);

        Thread threadProducerOne = new Thread(atmMoneyProducerOne);
        Thread threadConsumerOne = new Thread(atmMoneyConsumerOne);
        Thread threadProducerTwo = new Thread(atmMoneyProducerTwo);
        Thread threadConsumerTwo = new Thread(atmMoneyConsumerTwo);
        Thread threadProducerThree = new Thread(atmMoneyProducerThree);
        Thread threadConsumerThree = new Thread(atmMoneyConsumerThree);

        threadProducerOne.start();
        threadConsumerOne.start();
        threadProducerTwo.start();
        threadConsumerTwo.start();
        threadProducerThree.start();
        threadConsumerThree.start();

        while ((card.getBalance().get() > 0) && card.getBalance().get() < 1000) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        atmMoneyProducerOne.close();
        atmMoneyConsumerOne.close();
        atmMoneyProducerTwo.close();
        atmMoneyConsumerTwo.close();
        atmMoneyProducerThree.close();
        atmMoneyConsumerThree.close();
        if (card.getBalance().get() >= 1000) {
            System.out.println("The balance is more than 1000$. The program is stooped.");
        } else if (card.getBalance().get() <= 0) {
            System.out.println("There is no money left. The program is stopped");
        }
    }
}
