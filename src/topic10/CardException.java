package topic10;

public class CardException extends Exception {

    public CardException(String s) {
        super(s);
    }
}
