package topic10;

import java.util.concurrent.atomic.AtomicBoolean;

public class AtmProducer implements Runnable {
    private final Atm atmMoneyProducer;
    private final AtomicBoolean running = new AtomicBoolean(false);

    public AtmProducer(Atm atmMoneyProducer) {
        this.atmMoneyProducer = atmMoneyProducer;
    }


    @Override
    public void run() {
        running.set(true);
        while (running.get()) {
            try {
                add(100);
            } catch (CardException e) {
                Thread.currentThread().interrupt();
                running.set(false);
            }
        }
    }

    private synchronized void add(int money) throws CardException {
        atmMoneyProducer.addMoney(money);
        System.out.println("Producer"
                + Thread.currentThread().getName()
                + " added 100$. Card account balance : " + atmMoneyProducer.getCard().getBalance());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public void close() {
        running.set(false);
    }
}

