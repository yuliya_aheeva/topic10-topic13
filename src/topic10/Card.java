package topic10;

import java.util.concurrent.atomic.AtomicInteger;

public class Card {
    private final AtomicInteger balance;


    public Card(AtomicInteger balance) {
        this.balance = balance;
    }

    public AtomicInteger getBalance() {
        return balance;
    }

    public AtomicInteger addMoney(int money) throws CardException {
        if (checkBalance()) {
            balance.addAndGet(money);
        } else {
            throw new CardException("Card is more than 1000");
        }
        return balance;
    }

    public AtomicInteger withdrawMoney(int money) throws CardException {
        if (checkBalance()) {
            balance.addAndGet(money);
        } else {
            throw new CardException("Card is empty");
        }
        return balance;
    }

    private boolean checkBalance() {
        return balance.get() < 1000 && balance.get() > 0;
    }
}

